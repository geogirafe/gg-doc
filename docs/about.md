---
sidebar_position: 1
---

# About GeoGirafe

GeoGirafe is an flexible application to build online geoportals.  
Several demo instances of GeoGirafe can be tested here: [https://demo.geomapfish.dev/](https://demo.geomapfish.dev/).

GeoGirafe is the next version of the GeoMapFish viewer, currently under development. Have a look at the [Roadmap](https://doc.geomapfish.dev/img/timeline_gmf_ggf_en.png) and feel free to contact us for more information.

GeoGirafe is an open source project supported by the GeoMapFish community. [https://geomapfish.org](https://geomapfish.org)
