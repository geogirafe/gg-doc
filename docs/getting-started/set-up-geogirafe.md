# Set up a GeoGirafe client

If you're new to GeoGirafe, this guide is the perfect place to start.

:::tip

GeoGirafe and this tutorial are written in TypeScript. However, it doesn't mean your project must be in TypeScript. We use [Vite](https://vitejs.dev/) to bundle our project. Please ensure you follow the "[Getting Started](https://vitejs.dev/guide/)" guide for Vite before proceeding.

:::

## Requirements

* NodeJS version 20+
* GIT

## Install

GeoGirafe can be installed with just one command:

```sh
npm create @geogirafe/template@latest my-geogirafe-app
```

This will :
- create a folder `my-geogirafe-app` containing a minimal project of GeoGirafe.
- download all the dependencies and build the project

This will take 2-3 minutes, depending on your network speed.

Switch to the newly created repository. Then start the project :

```sh
cd my-geogirafe-app
npm start
```

That's it, you should now be able to see your first map, congratulations!

You can now start by reading the README file of the project, it contains the first explanations.
Each file in this project also contains comments. This will allow you to better understand how is working GeoGirafe, and how you can configure it to your needs, add your own layers, ...

If necessary, you can always get in touch with us on Dicord: https://discord.com/channels/1194934479282778122

Have a nice journey with GeoGirafe! :-)

