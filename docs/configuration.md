---
sidebar_position: 5
---

# Configuration

This section lists all the configuration entries available in GeoGirafe, and how to configure them.
The whole configuration is done in the `config.json` file.

Every configuration option that has no default value must be set properly to make the application work.

## Basemaps

The list of basemaps available in the application is loaded from the <code>themes.json</code> file.
In addition, you can use the `basemaps` section to configure additional options.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>show</code></td>
    <td><code>Boolean</code></td>
    <td>Display the basemap control or not. If set to <code>false</code>, the basemap controller won't be visible in the application.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>defaultBasemap</code></td>
    <td><code>String</code></td>
    <td>Name of the default basemap, that will be displayed when the application starts. This is the name of the background map available in <code>themes.json</code>.</td>
    <td><code>'Empty'</code></td>
    <td><code>background_color</code>, <code>swissimage</code></td>
  </tr>
  <tr>
    <td><code>OSM</code></td>
    <td><code>Boolean</code></td>
    <td>Enable or disable OpenStreetMap as an additional basemap layer.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>SwissTopoVectorTiles</code></td>
    <td><code>Boolean</code></td>
    <td>Enable or disable the VectorTiles layer from Swisstopo as an additional basemap layer.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>emptyBasemap</code></td>
    <td><code>Boolean</code></td>
    <td>Enable or disable the empty basemap.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## CSV

The `csv` section contains the configuration options to format CSV file to download.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>encoding</code></td>
    <td><code>String</code></td>
    <td>The encoding of the file.</td>
    <td><code>utf-8</code></td>
    <td><code>iso-8859-3</code></td>
  </tr>
  <tr>
    <td><code>extension</code></td>
    <td><code>String</code></td>
    <td>The extension of the file.</td>
    <td><code>.csv</code></td>
    <td><code>.txt</code></td>
  </tr>
  <tr>
    <td><code>includeHeader</code></td>
    <td><code>Boolean</code></td>
    <td>Include the header (columns titles).</td>
    <td><code>true</code></td>
    <td><code>false</code></td>
  </tr>
  <tr>
    <td><code>quote</code></td>
    <td><code>String</code></td>
    <td>The symbol to use to as simple quote.</td>
    <td><code>'</code></td>
    <td><code>"</code></td>
  </tr>
  <tr>
    <td><code>separator</code></td>
    <td><code>String</code></td>
    <td>The values separator.</td>
    <td><code>,</code></td>
    <td><code>;</code></td>
  </tr>
</table>

## Drawing

The `drawing` section contains the configuration options when drawing features on the map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>defaultFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when drawing features on the map.</td>
    <td><code>#6666ff7f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when drawing features on the map.</td>
    <td><code>#0000ff</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeWidth</code></td>
    <td><code>Number</code></td>
    <td>Default stroke width used when drawing features on the map.</td>
    <td><code>2</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultTextSize</code></td>
    <td><code>Number</code></td>
    <td>Default text size used when drawing features on the map.</td>
    <td><code>12</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultFont</code></td>
    <td><code>String</code></td>
    <td>Default font used when drawing features on the map.</td>
    <td><code>Arial</code></td>
    <td><code>Arial</code>, <code>Tahoma</code>, <code>Verdana</code></td>
  </tr>
  <tr>
    <td><code>defaultVertexRadius</code></td>
    <td><code>Number</code></td>
    <td>Default symbol size used to show feature vertices.</td>
    <td><code>8</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultVertexFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used to show feature vertices.</td>
    <td><code>#ffffffbf</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultVertexStrokeWidth</code></td>
    <td><code>Number</code></td>
    <td>Default stroke width used to show feature vertices.</td>
    <td><code>2</code></td>
    <td><code>any integer</code></td>
  </tr>
</table>

## General

The `general` section contains all the common configuration entries that can be used by any component.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>locale</code></td>
    <td><code>String</code></td>
    <td>Locale used when formatting numbers or dates in the application. For example this locale will be used to format the coordinates.</td>
    <td><code>en-US</code></td>
    <td><code>de-CH</code>, <code>fr-FR</code></td>
  </tr>
  <tr>
    <td><code>logLevel</code></td>
    <td><code>String</code></td>
    <td>Log level of the application. When defining a log level, all logs of this level and above will be displayed in the console.
    In development mode, the log level is forced to <code>debug</code>.</td>
    <td><code>warn</code></td>
    <td><code>debug</code>, <code>info</code>, <code>warn</code>, <code>error</code></td>
  </tr>
</table>

## GmfAuth

The `gmfauth` section is optional and contains configuration options for GeoMapFish Standard (legacy) authentication protocol.

If the geogirafe client is not running on the same domain as the GMF backend, the GMF Backend needs to be configured. Follow this steps to make it work:

1. You can manage CORS outside geoportal, for example with an lua script at the in the haproxy configuration. If you want to manage it within your geoportal, in the vars.yaml add CORS. Example:

```yaml
# Control the HTTP headers
headers:
  dynamic: &header {}
  index: *header
  api: *header
  apihelp: *header
  profile: *header
  raster: *header
  vector_tiles: *header
  error: *header
  themes: &auth_header
    cache_control_max_age: 600 # 10 minutes
    cache_control_max_age_nocache: 10 # 10 seconds, to avoid too many call
    access_control_max_age: 600 # 10 minutes
    access_control_allow_origin:
      - "{VISIBLE_WEB_PROTOCOL}://{VISIBLE_WEB_HOST}"
      - "https://app.localhost:8080"
      - "https://demo.geomapfish.dev"
    headers:
      Strict-Transport-Security: max-age=31536000; includeSubDomains
  config: *auth_header
  print: *auth_header
  fulltextsearch: {}
  mapserver: *auth_header
  tinyows: *auth_header
  layers: *auth_header
  shortener: *auth_header
  login: *auth_header
```

Theses vars need to be updated. At the bottom of the vars.yaml, add the update_paths to the existing list:

```yaml
update_paths:
  - headers.dynamic.headers
  - headers.index.headers
  - headers.api.headers
  - headers.apihelp.headers
  - headers.profile.headers
  - headers.raster.headers
  - headers.vector_tiles.headers
  - headers.error.headers
  - headers.themes.headers
  - headers.config.headers
  - headers.print.headers
  - headers.fulltextsearch.headers
  - headers.mapserver.headers
  - headers.tinyows.headers
  - headers.layers.headers
  - headers.shortener.headers
  - headers.login.headers
```

2. The frontend domain has to be allowed as referer in the `vars.yaml` file:

```yaml
authorized_referers:
  - "https://app.localhost:8080/"
  - "https://demo.geomapfish.dev/"
```

3. In your `env.project` The variable `AUTHTKT_SAMESITE` has to be set to `None`, to allow authentication cookies to be sent to the backend from another domain.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>URL to the GeoMapFish backend.</td>
    <td>-</td>
    <td><code>https://map.cartoriviera.ch</code></td>
  </tr>
  <tr>
    <td><code>checkSessionOnLoad</code></td>
    <td><code>Boolean</code></td>
    <td>Whether geogirafe should immediately check on page load whether the user is connected to the issuer (implies back and forth redirects to the issuer and geogirafe).</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>loginRequired</code></td>
    <td><code>Boolean</code></td>
    <td>Whether a login is required when the applicatoin starts. If `true`, the application will force the user to login before he can use the application.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## InfoWindow

The `infoWindow` section contains the configuration options for showing a window containing an iFrame.
It is currently used to display metadata, but can generally show any kind of information inside an iFrame.

Config  values can be defined as either strings containing CSS (including unit px, %, em, rem) or integers containing pixel values.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>defaultWindowWidth</code></td>
    <td><code>String | Integer</code></td>
    <td>Default width of the info window, in CSS units.</td>
    <td><code>'960px'</code></td>
    <td><code>'500px'</code>, <code>'20rem'</code>, <code>500</code></td>
  </tr>
  <tr>
    <td><code>defaultWindowHeight</code></td>
    <td><code>String | Integer</code></td>
    <td>Default height of the info window, in CSS units.</td>
    <td><code>'460px'</code></td>
    <td><code>'350px'</code>, <code>'10rem'</code>, <code>350</code></td>
  </tr>
  <tr>
    <td><code>defaultWindowPositionTop</code></td>
    <td><code>String | Integer</code></td>
    <td>Default top position of the info window, measured from the top map edge, in CSS units.</td>
    <td><code>'1rem'</code></td>
    <td><code>'10px'</code>, <code>'1rem'</code>, <code>10</code></td>
  </tr>
  <tr>
    <td><code>defaultWindowPositionLeft</code></td>
    <td><code>String | Integer</code></td>
    <td>Default left position of the info window, measured from the browser edge. in CSS units.</td>
    <td><code>'2rem'</code></td>
    <td><code>'20px'</code>, <code>'2rem'</code>, <code>20</code></td>
  </tr>
</table>

## Interface

The `interface` section contains the default settings for the layout and active component of the applications.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>defaultSelectionComponent</code></td>
    <td><code>String</code></td>
    <td>The name of the selection component to be displayed per default. This can be `window` or 'grid', or another value if you have a custom plugin. The corresponding component must also exist in your HTML and script code.</td>
    <td>window</td>
    <td>grid</td>
  </tr>
</table>

## Languages

The `languages` section contains an option for each available language in the application plus other options is an `Array` of `language`.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>translations</code></td>
    <td><code>Object</code></td>
    <td>An object containing the list of available translations identified by their shortname. The corresponding value is an array of links to the JSON files containing the translations as value.</td>
    <td>-</td>
    <td>

```json
"translations": {
  "de": ["de.json", "static/de.json"],
  "en": ["https://map.geo.bs.ch/static/X/en.json"],
  "fr": ["fr.json", "static/fr.json", "static/special_translations.json"]
}
```

  </td>
  </tr>
  <tr>
    <td><code>defaultLanguage</code></td>
    <td><code>String</code></td>
    <td>The shortname of the default language that will be used when the application starts.</td>
    <td>-</td>
    <td><code>en</code>, <code>fr</code>, <code>de</code></td>
  </tr>
</table>

## Lidar

The `lidar` section contains the configuration options for the LIDAR profile.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to a pytree service with LIDAR data and configuration.</td>
    <td><code>-</code></td>
    <td><code>https://sitn.ne.ch/pytree</code></td>
  </tr>
</table>

## Map

The `map` section contains the configuration of the `OpenLayers` map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>srid</code></td>
    <td><code>String</code></td>
    <td>Default SRID of the map.</td>
    <td>-</td>
    <td><code>EPSG:2056</code></td>
  </tr>
  <tr>
    <td><code>scales</code></td>
    <td><code>Array</code></td>
    <td>Array of scales allowed on the map.</td>
    <td>-</td>
    <td><code>[200000, 75000, 40000, 20000, 10000, 7500, 5000, 3500, 2000, 1000, 500, 200, 100, 50]</code></td>
  </tr>
  <tr>
    <td><code>startPosition</code></td>
    <td><code>String</code></td>
    <td>Initial center position when the application starts.</td>
    <td>-</td>
    <td><code>2611000,1267000</code></td>
  </tr>
  <tr>
    <td><code>startZoom</code></td>
    <td><code>Integer</code></td>
    <td>Initial zoom level when the application starts.</td>
    <td>-</td>
    <td><code>any valid integer for the map zoom level</code></td>
  </tr>
  <tr>
    <td><code>maxExtent</code></td>
    <td><code>String</code></td>
    <td>Maximum extent for the map. If not set, the map cover the entire world.</td>
    <td>-</td>
    <td><code>2583000,1235000,2650000,1291000</code></td>
  </tr>
  <tr>
    <td><code>constrainScales</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the map will only be allowed to used the specific scales defined in <code>maps.scales</code>.<br/>If <code>false</code>, any scale is allowed.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>showScaleLine</code></td>
    <td><code>Boolean</code></td>
    <td>Show scale line on the map, or not.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## Map3D

The `map3d` section contains the configuration of the `Cesium` globe.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>terrainUrl</code></td>
    <td><code>String</code></td>
    <td>URL for the 3D map terrain.</td>
    <td>-</td>
    <td><code>https://terrain100.geo.admin.ch/1.0.0/ch.swisstopo.terrain.3d/</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery</code></td>
    <td><code>Object</code></td>
    <td>See below</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>terrainImagery.url</code></td>
    <td><code>String</code></td>
    <td>The URL of your imagery provider (WMTS in your case), with template parameters as described in the <a href="https://cesium.com/learn/cesiumjs/ref-doc/UrlTemplateImageryProvider.html?classFilter=UrlTemplateImageryProvider#url" target="_bla^nk">Cesium documentation</a>.</td>
    <td>-</td>
    <td><code>https://wmts20.geo.admin.ch/1.0.0/ch.swisstopo.swissimage-product/default/current/4326/&#123;z&#125;/&#123;x&#125;/&#123;y&#125;.jpeg</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.srid</code></td>
    <td><code>Integer</code></td>
    <td>The SRID of the service used for terrainImagery. Only <code>4326</code> and <code>3857</code> are supported.</td>
    <td>4326</td>
    <td><code>4326</code>, <code>3857</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.minLoD</code></td>
    <td><code>Integer</code></td>
    <td>The minimal level-of-detail that the imagery provider supports, if not mentioned, when zooming very far, the map may try to load nonexistent tiles that cause network error.</td>
    <td>-</td>
    <td><code>any integer that represents a valid level-of-details</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.maxLoD</code></td>
    <td><code>Integer</code></td>
    <td>Same as <code>minLoD</code> but for maximal level-of-detail, to prevent loading nonexistent tiles when zooming very close.</td>
    <td>-</td>
    <td><code>any integer that represents a valid level-of-details</code></td>
  </tr>
  <tr>
    <td><code>terrainImagery.coverageArea</code></td>
    <td><code>Array</code></td>
    <td>The rectangle, in degree, that describe the area provided by your WMTS. This parameter prevents requesting tiles that do not exist on the WMTS.</td>
    <td>-</td>
    <td><code>[5.013926957923385, 45.35600133779394, 11.477436312994008, 48.27502358353741]</code></td>
  </tr>
  <tr>
    <td><code>tilesetsUrls</code></td>
    <td><code>Array</code></td>
    <td>Array of URLs for 3D map tilesets.</td>
    <td>-</td>
    <td><code>["https://3d.geo.bs.ch/static/tiles/a57b8783-1d7b-4ca9-b652-d4ead5436ead_2/tileset.json"]</code></td>
  </tr>
  <tr>
    <td><code>maximumZoomDistance</code></td>
    <td><code>Integer</code></td>
    <td>The maximum magnitude, in meters, of the camera position when zooming.</td>
    <td>30000</td>
    <td><code>25000</code></td>
  </tr>
</table>

## Metadata

The `metadata` section contains the configuration options for the Metadata URL. 
For setting window display options like width, height and position, see in topic `InfoWindow`.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>metadataUrlPrefix</code></td>
    <td><code>String</code></td>
    <td>URL Prefix to use when accessing metadata links, if there are defined with relative paths in <code>themes.json</code>.</td>
    <td><code>''</code></td>
    <td><code>https://map.geo.bs.ch</code></td>
  </tr>
</table>

## OAuth

The `oauth` section is optional and contains configuration options for Oauth Open ID connect (OIDC) authentication. In this model, the user is redirected to an `issuer` to prove his identity, redirecting back to geogirafe with a token. Geogirafe then sends this token to log in to GeoMapFish. This implementation should support any generic OIDC issuer. As of this writing it has been validated with Keycloak (used by Basel-Stadt).

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>issuer</code></td>
    <td><code>Object</code></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td><code>issuer.url</code></td>
    <td><code>String</code></td>
    <td>URL for the OIDC issuer's authorization endpoint.</td>
    <td>-</td>
    <td><code>https://auth.geo.bs.ch/auth/realms/geobs</code></td>
  </tr>
  <tr>
    <td><code>issuer.clientId</code></td>
    <td><code>String</code></td>
    <td>Client Identifier of the GeoMapFish client application.</td>
    <td>-</td>
    <td><code>MapBS-ui</code></td>
  </tr>
  <tr>
    <td><code>issuer.algorithm</code></td>
    <td><code>String</code></td>
    <td>OIDC algorithm oauth should use. Possible values: <code>oidc</code>, <code>oauth2</code></td>
    <td><code>oidc</code></td>
    <td><code>oidc</code>, <code>oauth2</code></td>
  </tr>
  <tr>
    <td><code>issuer.scope</code></td>
    <td><code>String</code></td>
    <td>The scope or scopes requested during the authentication process. Scopes determine the level of access and the type of information that the application can retrieve about the user. Default is <code>openid</code>, which is necessary for OIDC. You can include additional scopes like <code>profile</code> or <code>email</code> for more user information.</td>
    <td><code>openid</code></td>
    <td><code>openid</code>, <code>profile</code>, <code>email</code></td>
  </tr>
  <tr>
    <td><code>issuer.codeChallengeMethod</code></td>
    <td><code>String</code></td>
    <td>
      Specifies the method used for transforming the code challenge when using the Proof Key for Code Exchange (PKCE) extension. This enhances security during the authorization process.
    </td>
    <td><code>S256</code></td>
    <td><code>plain</code>, <code>S256</code></td>
  </tr>
  <tr>
    <td><code>issuer.checkSessionOnLoad</code></td>
    <td><code>Boolean</code></td>
    <td>Whether geogirafe should immediately check on page load whether the user is connected to the issuer (implies back and forth redirects to the issuer and geogirafe).</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>issuer.loginRequired</code></td>
    <td><code>Boolean</code></td>
    <td>Whether a login is required when the applicatoin starts. If `true`, the application will force the user to login before he can use the application.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>

  <tr>
    <td><code>geomapfish</code></td>
    <td><code>Object</code></td>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td><code>geomapfish.loginUrl</code></td>
    <td><code>String</code></td>
    <td>URL to send the issuer's token to login to GeoMapFish-Keycloak.</td>
    <td>-</td>
    <td><code>https://map.geo.test.bs.ch/KeycloakLogin</code></td>
  </tr>
  <tr>
    <td><code>geomapfish.logoutUrl</code></td>
    <td><code>String</code></td>
    <td>URL to end the session on GeoMapFish-Keycloak (note: doesn't end the issuer's session).</td>
    <td>-</td>
    <td><code>https://map.geo.test.bs.ch/KeycloakLogout</code></td>
  </tr>
  <tr>
    <td><code>geomapfish.userInfoUrl</code></td>
    <td><code>String</code></td>
    <td>URL to get user's details from GeoMapFish-Keycloak.</td>
    <td>-</td>
    <td><code>https://map.geo.test.bs.ch/loginuser</code></td>
  </tr>
  <tr>
    <td><code>geomapfish.anonymousUsername</code></td>
    <td><code>String</code></td>
    <td>Hitting the <code>geomapfish.userInfoUrl</code> endpoint when unlogged yields an anonymous user with a <code>username</code>. the <code>anonymousUsername</code> is used to check whether the user is unlogged on Geomapfish-Keycloak.</td>
    <td>-</td>
    <td><code>service-account-service_mapbs_anonymous</code></td>
  </tr>
</table>

## Offline

The `offline` section contains the configuration for the offline usage of GeoGirafe.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>downloadStartZoom</code></td>
    <td><code>Number</code></td>
    <td>The number of the zoom level from which downloading tiles for usage in offline mode will be allowed.</td>
    <td>-</td>
    <td><code>any number corresponding to a valid zoom level</code></td>
  </tr>
  <tr>
    <td><code>downloadEndZoom</code></td>
    <td><code>Number</code></td>
    <td>The zoom level to which tiles will be downloaded for an usage in offline mode.</td>
    <td>-</td>
    <td><code>any number corresponding to a valid zoom level</code></td>
  </tr>
</table>

## Print

The `print` section contains the configuration options for the print.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant Print Service.</td>
    <td>-</td>
    <td><code>https://map.geo.bs.ch/printproxy/</code></td>
  </tr>
  <tr>
    <td><code>formats</code></td>
    <td><code>String[]</code></td>
    <td>The desired print formats. The provided format must be supported by MapFishPrint. The order is kept in the dropdown.</td>
    <td><code>["png", "pdf"]</code></td>
    <td><code>["png", "pdf", "jpg"]</code></td>
  </tr>
  <tr>
    <td><code>defaultFormat</code></td>
    <td><code>String</code></td>
    <td>The print format selected by default. Only used if listed in <code>formats</code> and supported by MapFishPrint.</td>
    <td>-</td>
    <td><code>"pdf"</code></td>
  </tr>
  <tr>
    <td><code>layouts</code></td>
    <td><code>String[]</code></td>
    <td>A list of allowed layouts name. If configured, the layouts will be filtered using this value.</td>
    <td>-</td>
    <td><code>["1 A4 portrait", "2 A3 landscape"]</code></td>
  </tr>
  <tr>
    <td><code>defaultLayout</code></td>
    <td><code>String</code></td>
    <td>The print layout selected by default. Only used if the name match a layout.</td>
    <td>-</td>
    <td><code>"1 A4 portrait"</code></td>
  </tr>
  <tr>
    <td><code>scales</code></td>
    <td><code>Number</code></td>
    <td>A list of allowed scales for every layouts. If configured, the scales will be filtered using this value.</td>
    <td>-</td>
    <td><code>[50000, 25000, 10000, 2500, 1000]</code></td>
  </tr>
  <tr>
    <td><code>attributeNames</code></td>
    <td><code>String</code></td>
    <td>The print attributes (optional fields) to display as inputs in the panel. The order is kept in the panel.</td>
    <td>-</td>
    <td><code>["legend", "title", "comments"]</code></td>
  </tr>
  <tr>
    <td><code>legend</code></td>
    <td><code>Object</code></td>
    <td>The options for the print legend. See below.</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>legend.useExtent</code></td>
    <td><code>boolean</code></td>
    <td>Use or not the bbox to get the WMS legend. For QGIS server only.</td>
    <td><code>true</code></td>
    <td><code>false</code></td>
  </tr>
  <tr>
    <td><code>legend.showGroupsTitle</code></td>
    <td><code>boolean</code></td>
    <td>Display or not groups title in the legend. Switching to false is useful to obtains a "flat" legend.</td>
    <td><code>true</code></td>
    <td><code>false</code></td>
  </tr>
  <tr>
    <td><code>legend.label</code></td>
    <td><code>Object&lt;string&#44; boolean></code></td>
    <td>The key is the server type (MapServer, QGIS, etc.), if the value is false the name of the layer will be not displayed. This is used to avoid duplicated title, as text and in the legend image.</td>
    <td>-</td>
    <td><code>&#123;"mapserver": false&#125;</code></td>
  </tr>
  <tr>
    <td><code>legend.params</code></td>
    <td><code>Object&lt;string&#44; &lt;Object&lt;string&#44; unknown&gt;&gt;</code></td>
    <td>The key is the server type (MapServer, QGIS, etc.) or image for an URL from a metadata. The value is some additional parameters set in the query string.</td>
    <td>-</td>
    <td><code>&#123;"mapserver": &#123;"filter": "house"&#125;&#125;</code></td>
  </tr>
  <tr>
    <td><code>customScale</code></td>
    <td><code>Boolean</code></td>
    <td>Display or not the custom scale field. NB: Set <code>map.constrainScales = false</code> to allow any map scale to be displayed (as custom scales is not in the defined one).</td>
    <td><code>false</code></td>
    <td><code>true</code></td>
  </tr>
</table>

## Projections

The `projections` section contains the list of projection that are available in the application.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>projections</code></td>
    <td><code>String</code></td>
    <td>An array containing the list of <code>EPSG</code> coordinate systems with their name.</td>
    <td>-</td>
    <td>

```json
"projections": {
  "EPSG:3857": "W-M",
  "EPSG:4326": "WGS84",
  "EPSG:2056": "LV95"
}
```

  </td>
  </tr>
</table>

## Query

The `query` section contains the configuration options for the query result window.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>legacy</code></td>
    <td><code>Boolean</code></td>
    <td>Allow less restricted parsing of query result value, allowing some JavaScript events to be whitelisted.<br/>Currently supports: <br/> - <code>onclick</code></td>
    <td><code>false</code></td>
    <td><code>true</code></td>
  </tr>
</table>

## Search

The `search` section contains the configuration options for the search.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant Search Service. This service has to be compatible with the format of the GeoMapFish Search-Service. The link should contain the following strings:<br/>- <code>###SEARCHTERM###</code>: will be replaced by the text to search.<br/>- <code>###SEARCHLANG###</code>: will be replaced by the search language.</td>
    <td>-</td>
    <td><code>https://map.geo.bs.ch/search?limit=90&partitionlimit=15&interface=desktop&query=###SEARCHTERM###&lang=###SEARCHLANG###</code></td>
  </tr>
  <tr>
    <td><code>objectPreview</code></td>
    <td><code>boolean</code></td>
    <td>Activate or not the object preview on the map when navigating through the search results.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>layerPreview</code></td>
    <td><code>boolean</code></td>
    <td>Activate or not the layer preview on the map when navigating through the search results.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>minResolution</code></td>
    <td><code>number</code></td>
    <td>Configure the minimum resolution to zoom to when searching for an object. Is the object is very small (a point for example), 
    the zoom will stop to this resolution.</td>
    <td><code>0.5</code></td>
    <td><code>any number</code></td>
  </tr>
  <tr>
    <td><code>defaultFillColor</code></td>
    <td><code>String</code></td>
    <td>Define the default fill color of the polygon features of the search results.</td>
    <td><code>#3388ff7f</code></td>
    <td><code>-</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Define the default stroke color of the line and polygon features of the search results.</td>
    <td><code>#3388ff</code></td>
    <td><code>-</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeWidth</code></td>
    <td><code>number</code></td>
    <td>Define the default stroke width of the line and polygon features of the search results.</td>
    <td><code>2</code></td>
    <td><code>any number</code></td>
  </tr>
  <tr>
    <td><code>paintSearchResults</code></td>
    <td><code>boolean</code></td>
    <td>Configure if the user can choose the color of the search results in the user interface.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
</table>

## Selection

The `selection` section contains the configuration options for the selected/focused features on the map.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>maxFeature</code></td>
    <td><code>Integer</code></td>
    <td>Limit of requested object for every single WFS request.</td>
    <td><code>300</code></td>
    <td><code>100</code></td>
  </tr>
  <tr>
    <td><code>defaultFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when selecting an object in the map.</td>
    <td><code>#ff66667f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when selecting an object in the map.</td>
    <td><code>#ff3333</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>defaultStrokeWidth</code></td>
    <td><code>Integer</code></td>
    <td>Default stroke width used when selecting an object in the map.</td>
    <td><code>4</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>highlightFillColor</code></td>
    <td><code>String</code></td>
    <td>Default fill color used when highlighting or focusing an object in the map.</td>
    <td><code>#ff33337f</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>highlightStrokeColor</code></td>
    <td><code>String</code></td>
    <td>Default stroke color used when highlighting or focusing an object in the map.</td>
    <td><code>#ff0000</code></td>
    <td><code>any color value in #RGBA format</code></td>
  </tr>
  <tr>
    <td><code>highlightStrokeWidth</code></td>
    <td><code>Integer</code></td>
    <td>Default stroke width used when highlighting or focusing an object in the map.</td>
    <td><code>4</code></td>
    <td><code>any integer</code></td>
  </tr>
</table>

## Share

The `share` section contains the configuration options for the short links.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>service</code></td>
    <td><code>String</code></td>
    <td>Type of ShortUrl service we want to use. Supported configuration are:<br/>- <code>gmf</code>: the GeoMapFish backend shortUrl service will be used.<br/>- <code>lstu</code>: the open-source service Let's Shorten That URL will be used.</td>
    <td><code>gmf</code></td>
    <td><code>gmf</code>, <code>lstu</code></td>
  </tr>
  <tr>
    <td><code>createUrl</code></td>
    <td><code>String</code></td>
    <td>The URL that will be used to create the shortUrl.</td>
    <td>-</td>
    <td><code>https://lstu.fr/a</code></td>
  </tr>
</table>

## Themes

The `themes` section contains the configuration options linked to the themes.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>url</code></td>
    <td><code>String</code></td>
    <td>Link to the GeoMapFish-Compliant <code>themes.json</code></td>
    <td>-</td>
    <td><code>Mock/themes.json</code>, <code>https://map.geo.bs.ch/themes?background=background&interface=desktop</code></td>
  </tr>
  <tr>
    <td><code>defaultTheme</code></td>
    <td><code>String</code></td>
    <td>Name of the default theme, that will be automatically loaded on application start.</td>
    <td><code>''</code></td>
    <td><code>main</code>, <code>cadastre</code></td>
  </tr>
  <tr>
    <td><code>imagesUrlPrefix</code></td>
    <td><code>String</code></td>
    <td>URL Prefix to use when accessing themes images, if there are defined with relative paths in <code>themes.json</code>.</td>
    <td><code>''</code></td>
    <td><code>https://map.geo.ti.ch</code></td>
  </tr>
  <tr>
    <td><code>showErrorsOnStart</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the errors listed in <code>themes.json</code> will be displayed when the application starts. If <code>false</code>, they won't be displayed.</td>
    <td><code>false</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>selectionMode</code></td>
    <td><code>String</code></td>
    <td>If <code>replace</code>, only one theme can be active at the same time, and during the selection of a new theme the current theme is replaced. If <code>add</code>, many themes can be active at the same time, and selecting a new theme adds it at the beginning of the tree view.</td>
    <td><code>replace</code></td>
    <td><code>replace</code>, <code>add</code></td>
  </tr>
</table>

## Treeview

The `treeview` section contains the configuration options for the layertree.

<table>
  <tr>
    <th>Name</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
    <th>Examples</th>
  </tr>
  <tr>
    <td><code>hideLegendWhenLayerIsDeactivated</code></td>
    <td><code>Boolean</code></td>
    <td>If <code>true</code>, the legend of a layer will automatically be hidden if the layer is not activated. If <code>false</code>, the legend will remain visible even if the layer is deactivated.</td>
    <td><code>true</code></td>
    <td><code>true</code>, <code>false</code></td>
  </tr>
  <tr>
    <td><code>defaultIconSize</code></td>
    <td><code>Object</code></td>
    <td>The size of the legend icons (getLegend WMS Url) can be configured here with the <code>width</code> and <code>height</code> properties. Defaults to 20 x 20 if not set.</td>
    <td>-</td>
    <td>-</td>
  </tr>
  <tr>
    <td><code>defaultIconSize.width</code></td>
    <td><code>Integer</code></td>
    <td>Width of the icon that will be used for legend icons</td>
    <td><code>20</code></td>
    <td><code>any integer</code></td>
  </tr>
  <tr>
    <td><code>defaultIconSize.height</code></td>
    <td><code>Height</code></td>
    <td>Width of the icon that will be used for legend icons</td>
    <td><code>20</code></td>
    <td><code>any integer</code></td>
  </tr>
</table>
