# Translations

Geogirafe uses the [i18next internationalization framework](https://www.i18next.com/) to translate user interface elements.

The `i18n` attribute in html elements is a reference to a variable in the i18n translation files located in the `src/assets/i18n/` folder.
In the preconfigured instance we are using, there are 4 files with the core translations: `de.json` (German), `en.json` (English), `fr.json` (French), `it.json` (Italian). Additional local or remote translation files can be included as needed.

To access translation variables (in component.ts or template.html), Geogirafe also provides a `I18nManager` class with a `getTranslation()` method, e.g.:

```html
<div>${this.i18nManager.getTranslation('color_giraffe')}</div>
```

The list of translations and source files must be configured in `public/config.json`, e.g.:

```json
"languages": {
  "translations": {
    "de": ["i18n/de.json", "i18n/custom-translation-de.json"],
    "en": ["i18n/en.json"],
    "fr": ["i18n/fr.json", "https://sitn.ne.ch/static/dummy/fr.json"],
    "it": ["i18n/it.json"]
  },
  "defaultLanguage": "en"
},
```

See the documentation on [Configuration](../../docs/configuration#languages) for more details.
