# Dialogs and Windows

GeoGirafe offers a few tools to show windowed communication and information to the user in a modal (blocking) and non-modal (non-blocking) way.

Because these windows and dialogs belong to the `window` namespace, they can be opened from anywhere inside the application, for example in a custom component.

:::tip[Translations]

Remember to translate all texts of the dialog.

:::


### gAlert (blocking)
Use `gAlert` to inform the user about something important like a warning or an error.
Specify a message and an optional title when calling it.

`gAlert` returns a promise that always resolves to `true`.
``` ts
public doAThing() {
    try {
        doTheThing();
    } catch {
        void window.gAlert('There was a problem with the thing', 'Oopsie');
    }
}
```

### gConfirm (blocking)
Use `gConfirm` to ask the user a yes/no question. Specify a message and an optional title.
The function returns a promise that resolves to a boolean indicating the users decision (yes/no).

Make sure to use this dialog with an `async` / `await` structure to wait for the users decision.
``` ts
public onDelete() {
    const confirm = await window.gConfirm('Do you want to delete this data?', 'Delete data');
    if (confirm) {
        deleteData();
    }
}
```

### gPrompt (blocking)
Use `gPrompt` to collect a text answer from the user.
Specify a message, optional title and optional placeholder text, that will be displayed above the input field.

The function returns a promise that resolves with the entered text or `false`, if the user canceled the dialog.

Use the `async` / `await` structure to catch the users decision.
``` ts
public async onCreate() {
    const nameOfThing = await window.gPrompt('Give a name to the new thing', 'Add a Thing', 'Enter a name...');
    if (nameOfThing) {
        createTheThing();
    }
}
```


### gOpenWindow (non-blocking)
The `gOpenWindow` function is a non-blocking window of customizable size that loads additional content inside an `iframe`.

Specify a title and URL, and optionally the width, height and left and top position of the window. 
The styling options can be given as CSS strings (`'500px'`, `'30%'`, `'5em'`) or as integer pixel values (`500`).

The window stays open until the user closes it or another `gOpenWindow` is opened. Only one window can be visible at a time.

``` ts
public onShowAdditionalInfo() {
    window.gOpenWindow('Additional info', 'https://example.com/info', '500px', '300px', 20, 20);
}
```
