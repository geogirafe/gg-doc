# Permalink

GeoGirafe has permalink parameters used to manage map position and some extra options.
This allow to load the project to a specific location and zoom, as well that manage tooltip and cross-hair features.
Position x, y, and zoom will change with the user moves in or out, and pan the map.

The following parameters are implemented:

<table>
  <tr>
    <th>Name</th>
    <th>Description</th>
    <th>Optional</th>
    <th>Type</th>
  </tr>
  <tr>
    <td>map_x</td>
    <td>X coordinate in the local projection</td>
    <td>false</td>
    <td>number</td>
  </tr>
  <tr>
    <td>map_y</td>
    <td>Y coordinate in the local projection</td>
    <td>false</td>
    <td>number</td>
  </tr>
  <tr>
    <td>map_zoom</td>
    <td>Zoom level of the map</td>
    <td>false</td>
    <td>number</td>
  </tr>
  <tr>
    <td>map_crosshair</td>
    <td>Display or not a crosshair at map center</td>
    <td>true</td>
    <td>boolean</td>
  </tr>
  <tr>
    <td>map_tooltip</td>
    <td>Display a tooltip text at map center</td>
    <td>true</td>
    <td>string</td>
  </tr>
</table>

## map_x, map_y, map_zoom

The default values of theses parameters are configured in the `map.startZoom` and `map.startPosition` configuration to set the OpenLayers view.
The values for x and y are in the local projection set in `map.srid`.

## map_crosshair

This parameter use the "selection" color present in the user config panel.
It's possible to change it selecting appriopriate color there.

![Crosshair color](../../static/img/crosshair_color.png)
