# Dev guidelines
_Guidelines and pointers for developers working on the GeoGirafe project on GitLab. 
For any additional questions, don't hesitate to ask us directly on the [discord server](https://discord.gg/kdrXjaqBbH)._


## Choosing an issue

When choosing an issue on [GitLab](https://gitlab.com/geogirafe/gg-viewer) to work on, follow this priority:

1. issues with your dev partner tag, see [dev partners issue board](https://gitlab.com/geogirafe/gg-viewer/-/boards/7863361)
2. issues in current milestone
3. issues with tag `gg.ready`

## Tagging and handling issues

The PSC uses issue boards to discuss current progress and test MRs (merge requests).
It's important to update the state of the current issue so all project members are informed of the project progress.

:::important[Issue state]
* Tag your issue with `gg.in-progress` and `gg.review` respectively
* Moving the issue on the board will automatically update its tags
* MRs don't have to be tagged
:::

### 1. When starting on an issue

*   assign yourself to the issue
*   on the issue board, move the issue from `ready` into `progress` column (remove tag `gg.ready`, add `gg.in-progress`)
*   create an MR with status `draft`

### 2. When finished

*   go through the MR checklist
*   move issue into `review` column (remove tag `gg.in-progess` , add `gg.review`)
*   in the MR, remove `draft` status and add a reviewer (see more info in the next chapter)

### 3. When MR is merged

*   Usually, the reviewer or PSC will close your issue. if not, feel free to close it
*   Make sure there are no open points left in the issue or comments

## Who to choose as reviewer?

* People with role `Maintainer` or `Owner` can review and merge MRs 
* Good rule of thumb when choosing a reviewer:
  * Look at the component or tool you worked on and choose the maintainer / owner who worked on the code before (git blame)
  * Check who wrote the initial issue

## Code quality

### Doc strings

* Doc strings aren't mandatory
* Write doc strings if the piece of code isn't self-explanatory at first glance, e.g. classes or longer functions
* Write doc strings for often used public methods
* Write doc strings where additional context and explanations are needed
* Docstrings are used to auto-generate the [docs](https://doc.geomapfish.dev/code/index.html)

### Unit tests
*  Add unit tests at your own discretion
*  Generally, core functionality (e.g. code in `tools` directory) should be covered by tests
